<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use DateTimeImmutable;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {

        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadBlogPosts($manager);
        $this->loadComments($manager);
    }

    public function loadBlogPosts(ObjectManager $manager)
    {
        /**
         * @var $user User
         */
        $user = $this->getReference('user_admin');

        $blogPost = new BlogPost();
        $blogPost->setTitle('A first new post!');
        $blogPost->setPublished(new DateTimeImmutable('2019-09-01 13:00:00'));
        $blogPost->setContent('Content first post!');
        $blogPost->setAuthor($user);
        $blogPost->setSlug('a-first-new-post');

        $manager->persist($blogPost);

        $blogPost = new BlogPost();

        $blogPost->setTitle('A second new post!');
        $blogPost->setPublished(new DateTimeImmutable('2019-09-01 13:00:00'));
        $blogPost->setContent('Content second post!');
        $blogPost->setAuthor($user);
        $blogPost->setSlug('a-second-new-post');

        $manager->persist($blogPost);

        $manager->flush();
    }

    public function loadComments(ObjectManager $manager)
    {

    }

    public function loadUsers(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('admin');
        $user->setEmail('admin@email.com');
        $user->setName('Yukhnevich Artur');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'secret'
        ));

        $this->addReference('user_admin', $user);

        $manager->persist($user);

        $manager->flush();
    }
}
